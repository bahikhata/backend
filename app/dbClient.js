var mongoose = require('mongoose');
var models = require('../models/model');
var multer = require('multer'); // v1.0.5
var upload = multer();
var Promise = require('mpromise');
var promise = new Promise;

module.exports.addTransaction = (req,res,next) => {
  var transaction = new models.Transaction();
  transaction.amount = req.body.amount;
  transaction.date = req.body.date;
  transaction.dueDate = req.body.dueDate;
  transaction.detail = req.body.detail;
  transaction.mode = req.body.mode;
  transaction.cancelled = req.body.cancelled;
  transaction.image = req.body.image;
  transaction.account_id = req.body.account_id;
  transaction.user_id = req.body.user_id;
  transaction.save(function(err,doc){
    if(err){
      res.status(500).send(err);
    }
    else{
      res.status(200).send(doc);
    }
  });
}

module.exports.getTransactions = (req,res,next) => {
  //{amount:3000}
  var query = models.Transaction
                .find()
                //.sort({'amount':-1,"detail":1})
                .limit(parseInt(req.query.limit))
                .skip(parseInt(req.query.limit) * (parseInt(req.query.page)-1));

  query.exec(function(err,transaction){
    if(err){
      res.status(500).send(err);
    }
    else{
      res.status(200).send(transaction);
    }
  });
}

module.exports.getTransactionById = (req,res,next) => {
  models.Transaction.findOne({_id:req.params.id},function(err,transaction){
    if(err){
      res.status(500).send(err);
    }
    else{
      res.status(200).send(transaction);
    }
  });
}

module.exports.updateTransaction = (req,res,next) => {
  var transaction = new models.Transaction();
  transaction.amount = 5000;
  transaction.detail = "Yo!!";

  models.Transaction.findOneAndUpdate({_id:req.params.id},
    {$set:{amount:2000,detail:"Yo"}},
    {new:true},
    function(err,doc){
      if(err){
        res.status(500).send(err);
      }
      else{
        res.status(200).send(doc);
      }
  });
}

module.exports.deleteTransaction = (req,res,next) => {
  models.Transaction.findOneAndRemove({_id:req.params.id},function(err,doc){
    if(err){
      res.status(500).send(err);
    }
    else{
      res.status(204).send("Deleted");
    }
  });
}

// Accounts Functions
module.exports.addAccount = (req,res,next) => {
  var account = new models.Account();
  account.name = req.body.name;
  account.phone = req.body.phone;
  account.category = req.body.category;
  account.cancelled = req.body.cancelled;
  account.balance = req.body.balance;
  account.user_id = req.body.user_id;
  account.save(function(err,doc){
    if(err){
      res.status(500).send(err);
    }
    else{
      res.status(200).send(doc);
    }
  });
}

module.exports.getAccounts = (req,res,next) => {
  //{amount:3000}
  var query = models.Account
                .find()
                //.sort({'amount':-1,"detail":1})
                .limit(parseInt(req.query.limit))
                .skip(parseInt(req.query.limit) * (parseInt(req.query.page)-1));

  query.exec(function(err,account){
    if(err){
      res.status(500).send(err);
    }
    else{
      res.status(200).send(account);
    }
  });
}

module.exports.getAccountById = (req,res,next) => {
  models.Account.findOne({_id:req.params.id},function(err,account){
    if(err){
      res.status(500).send(err);
    }
    else{
      res.status(200).send(account);
    }
  });
}

module.exports.updateAccount = (req,res,next) => {
  var account = new models.account();
  account.amount = 5000;
  account.detail = "Yo!!";

  models.Account.findOneAndUpdate({_id:req.params.id},
    {$set:{amount:2000,detail:"Yo"}},
    {new:true},
    function(err,doc){
      if(err){
        res.status(500).send(err);
      }
      else{
        res.status(200).send(doc);
      }
  });
}

module.exports.deleteAccount = (req,res,next) => {
  models.Account.findOneAndRemove({_id:req.params.id},function(err,doc){
    if(err){
      res.status(500).send(err);
    }
    else{
      res.status(204).send("Deleted");
    }
  });
}

// Loan Functions
module.exports.addLoan = (req,res,next) => {
  var loan = new models.Loan();
  loan.amount = req.body.amount;
  loan.contact = req.body.contact;
  loan.response = req.body.response;
  loan.user_id = req.body.user_id;

  loan.save(function(err,doc){
    if(err){
      res.status(500).send(err);
    }
    else{
      res.status(200).send(doc);
    }
  });
}

module.exports.getLoans = (req,res,next) => {
  //{amount:3000}
  var query = models.Loan
                .find()
                //.sort({'amount':-1,"detail":1})
                .limit(parseInt(req.query.limit))
                .skip(parseInt(req.query.limit) * (parseInt(req.query.page)-1));

  query.exec(function(err,loan){
    if(err){
      res.status(500).send(err);
    }
    else{
      res.status(200).send(loan);
    }
  });
}

module.exports.getLoanById = (req,res,next) => {
  models.Loan.findOne({_id:req.params.id},function(err,loan){
    if(err){
      res.status(500).send(err);
    }
    else{
      res.status(200).send(loan);
    }
  });
}

module.exports.updateLoan= (req,res,next) => {
  var loan = new models.Loan();
  loan.amount = 5000;
  loan.detail = "Yo!!";

  models.Loan.findOneAndUpdate({_id:req.params.id},
    {$set:{amount:2000,detail:"Yo"}},
    {new:true},
    function(err,doc){
      if(err){
        res.status(500).send(err);
      }
      else{
        res.status(200).send(doc);
      }
  });
}

module.exports.deleteLoan = (req,res,next) => {
  models.Loan.findOneAndRemove({_id:req.params.id},function(err,doc){
    if(err){
      res.status(500).send(err);
    }
    else{
      res.status(204).send("Deleted");
    }
  });
}

module.exports.testSync = (req,res,next) => {
  console.log(req);
  res.json({"result":true});
}

module.exports.admin = (req,res,next) => {
    res.render('new', { title: 'Bollywood, TV Series, Jokes' });
};

module.exports.addHome = (req,res,next) => {
    //Add content to database.
    console.log(req.body);
    var content = new models.Content();
    content.title = req.body.title;
    content.content = req.body.content;
    content.author = req.body.author;

    content.save(function(err,doc){
      if(err){
        res.status(500).send(err);
      }
      else{
        res.status(200).send(doc);
      }
    });
};
