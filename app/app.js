exports.db = require('./dbClient');
/*exports.home = (req,res,next) => {
    //res.sendFile(__public__+'index.html');
    res.render('index', { title: 'Bahi Khata' });
};*/
/*const AUTH_KEY = "303034AxCR0x5DGYK5dc6d39d";
const MSG_TEMPLATE = "5dc94d76d6fc0578bc42c4e7";
const SENDER_ID = "MSGIND";
const ROUTE_ID = "5ee61f29d6fc050bad1a8c9c";
const mobilenumber = "917456883325";

const sendSmsService = require('msg91-sdk').SendSmsService;
const sendSms = new sendSmsService(AUTH_KEY, MSG_TEMPLATE);

const params = {
	param_one: "Bahi Khata"
}

sendSms.sendSMSFlow(mobilenumber, MSG_TEMPLATE, params).then(() => {
	console.log("OTP Sent successfully !");
}).catch((error) => {
	console.log(JSON.stringify(error));
});
*/
/*const sendOtpService = require('msg91-sdk').SendOtpService;


const sendOtp = new sendOtpService(AUTH_KEY, MSG_TEMPLATE);
sendOtp.otpLength = 6;
sendOtp.otpExpiry = 5;

const aOptions = {
	mobile: "917456883325"
};

sendOtp.sendOTP(aOptions).then(() => {
	console.log('sent Otp');
}).catch((error) => {
	console.log(JSON.stringify(error));
});
*/
const express = require('express');
const axios = require('axios');
const app = express();
const PORT = 3030;
const AUTH_KEY = "303034AxCR0x5DGYK5dc6d39d";
const MSG_TEMPLATE = "5dc94d76d6fc0578bc42c4e7";
const MOBILE = "917456883325";
const BASE_API = "https://api.msg91.com/api/v5/otp";

app.listen(PORT, () => console.log('App listening at : ' + PORT));

app.get('/sendOTP', (req,res) => {
	axios.get(BASE_API+"?authkey=" + AUTH_KEY + "&template_id=" + MSG_TEMPLATE + "&mobile=" + MOBILE)
		.then((response) => {
			if(response.status == 200){
				res.send('OTP sent');
			}
		}).catch((error) => {
			res.send(JSON.stringify(error));
		});
});

app.get("/verifyOTP", (req,res)=> {
	const OTP = 3393;
	axios.get(BASE_API+"/verify?mobile=" + MOBILE + "&otp="+ OTP + "&authkey=" + AUTH_KEY)
		.then((response) => {
			if(response.status == 200){
				res.send(response.data);
			}
		}).catch((error) => {
			res.send(JSON.stringify(error));
		});
});
