var express = require('express');
var router = express.Router();
var path = require('path');
var constant = require('../app/constant');
var app = require('../app/app');
var auth = require('./auth');

var databaseUri = process.env.DATABASE_URI || constant.DATABASE_URI_DEV;
if (!databaseUri) {
  console.log('DATABASE_URI not specified, falling back to localhost.');
}

/* GET home page. */

router.get('/', function(req, res, next) {
  res.render('index', {title: ''});
});

router.get('/new', app.db.admin);
router.post('/new', app.db.addHome);

//router.get('/api/profile',app.parse.parseSample);

router.post('/login',auth.login);
router.post('/signup',auth.signup);
router.post('/logout',auth.logout);

router.get('/v1/transaction',app.db.getTransactions);
router.get('/v1/transaction/:id',app.db.getTransactionById);
router.post('/v1/transaction',app.db.addTransaction);
router.put('/v1/transaction/:id',app.db.updateTransaction);
router.delete('/v1/transaction/:id',app.db.deleteTransaction);

router.get('/v1/account',app.db.getAccounts);
router.get('/v1/account/:id',app.db.getAccountById);
router.post('/v1/account',app.db.addAccount);
router.put('/v1/account/:id',app.db.updateAccount);
router.delete('/v1/account/:id',app.db.deleteAccount);

router.get('/v1/loan',app.db.getLoans);
router.get('/v1/loan/:id',app.db.getLoanById);
router.post('/v1/loan',app.db.addLoan);
router.put('/v1/loan/:id',app.db.updateLoan);
router.delete('/v1/loan/:id',app.db.deleteLoan);

router.post('/test/sync',app.db.testSync);
router.get('/test/sync',app.db.testSync);



module.exports = router;
