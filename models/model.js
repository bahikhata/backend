var mongoose = require('mongoose');
var DATABASE_URI = require('../app/constant').DATABASE_URI_DEV;

var schema = require('./schema');

mongoose.connect(DATABASE_URI);

exports.Transaction = mongoose.model('Transaction',schema.transactionSchema);
exports.Account = mongoose.model('Account',schema.accountSchema);
exports.Loan = mongoose.model('Loan',schema.loanSchema);
exports.User = mongoose.model('User',schema.userSchema);
