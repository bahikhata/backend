var Schema = require('mongoose').Schema;

var userSchema = new Schema({
  username: { type: String, unique: true },
  password: String,
  name: String,
  phone: String,
  authData: String,
  digitsSession: Number,
  email: String
});

var transactionSchema = new Schema({
  amount: Number,
  date: { type: Date, default: Date.now },
  dueDate: Date,
  detail: String,
  mode: Number,
  cancelled: Boolean,
  image: String,
  account_id: String,
  user_id: String
});

var accountSchema = new Schema({
  name  : String,
  phone : String,
  category: String,
  cancelled: Boolean,
  balance: Number,
  user_id: String
});

var loanSchema = new Schema({
  amount: Number,
  contact: String,
  response: String,
  user_id: String
});

module.exports = {userSchema,transactionSchema,accountSchema,loanSchema};
